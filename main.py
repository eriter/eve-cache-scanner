''' Main entry-point of the application '''
import sqlite3
import logging
import json
import os
from datetime import timedelta

from web import app
from lib.cache_watcher import EveCacheObserver
from lib.api_watcher import EveApiObserver

def main():
  '''
  Starts all three parts of the application:
    Web server
    Cache watcher
    API updater (fetching transactions)
  '''
  # Configure logging
  logging.basicConfig(
    level=logging.INFO,
    format='[%(levelname)s: %(asctime)s] %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S'
  )

  connect = lambda: sqlite3.connect(
    'db.sqlite3',
    detect_types=sqlite3.PARSE_DECLTYPES
  )

  # Read settings
  observers = []
  try:
    settings = json.load(open('settings.json'))
    eve_installation_directory = settings.get('eve_installation_directory', '')

    if os.path.exists(eve_installation_directory):
      cache_observer = EveCacheObserver(connect, eve_installation_directory)
      app.market_order_updater = cache_observer
      observers.append(cache_observer)
    else:
      logging.info("Could not find eve installation directory," \
                   "can't start cache reader.")

    if 'api_key' in settings:
      api_key = (
        settings['api_key']['id'],
        settings['api_key']['verification_code'],
      )
      observers.append(EveApiObserver(connect, api_key))
    else:
      logging.info("Could not find EVE api key, can't fetch transactions.")

  except IOError:
    logging.info("Could not find/read settings.json, starting web server only.")

  for observer in observers:
    observer.start()

  # Start web server (blocks)
  app.run(host='0.0.0.0', threaded=True)

  # Before exit
  for observer in observers:
    observer.stop()

if __name__ == "__main__":
  main()
