/*global MarketScanner */
(function ($) {
  'use strict';

  $(function () {
    // Create the tree
    $.getJSON('/tree').success(function (data) {
      new MarketScanner($('#market-scanner'), data.tree, data.parents);
    });

    $('input:radio[name="market-updater"]').click(function () {
      var value = $('input:radio[name="market-updater"]:checked').val();

      $.ajax({
        url: '/market_updater/' + value,
        type: 'PUT'
      });
    });
  });
})(jQuery);
