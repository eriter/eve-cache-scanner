/*global CCPEVE */
$.jstree._themes = 'static/vendor/js/themes/';

(function ($) {
  'use strict';
  /*
    Requires the following controls to be present to render correctly:
      spans:
        #market-scanner-timeleft
        #market-scanner-progress
        #market-scanner-next-item
      input of type range:
        #market-scanner-interval
      label:
        #market-scanner-interval-label
      input type button:
        #market-scanner-start
        #market-scanner-reset
        #market-scanner-forward
        #market-scanner-back
        #market-scanner-item-slider
        #market-scanner-uncheck-all
      div (container for the tree)
        #market-scanner
  */

  window.MarketScanner = function (tree_container, market_data, parent_lookup) {
    this.parent_lookup = parent_lookup;
    this.market_data = market_data;

    this.tree = $(tree_container).jstree({
      // Plugins to be included for this tree instance
      'plugins' : ['checkbox', 'json_data', 'ui', 'themes'],

      // Options
      'core':  {animation: 0},
      'themes': {theme: 'default', icons: false, dots: false},
      'checkbox': {override_ui: true, checked_parent_open: false},
      'json_data' : {
        'data': market_data,
        progressive_render: true
      }
    });

    this.scanner = {
      index: 0,
      timer: null,
      list: [],
      state: 'idle',
      interval: parseInt($('#market-scanner-interval').attr('value'), 10) * 100
    };

    var self = this;
    var triggerItemCount = function () {
      var count = 0;

      self.tree.jstree('get_checked').each(function (index, element) {
        var typeID    = $(element).attr('typeid'),
            itemCount = $(element).attr('itemcount');
        count += (typeID === undefined ? parseInt(itemCount, 10) : 1);
      });

      $(self).trigger('item_count_changed', count);
    };

    // On changing the interval slider
    $('#market-scanner-interval').change(function () {
      self.scanner.interval = parseInt($('#market-scanner-interval').attr('value'), 10) * 100;
      $('#market-scanner-interval-label').text(($('#market-scanner-interval').attr('value') / 10) + 's');

      if (self.scanner.state === 'idle') {
        self.scanner.timeSinceLastUpdate = self.scanner.interval;
      }

      self.updateProgress();
    });

    // On clicking 'start'
    $('#market-scanner-start').click(function () {
      if (self.scanner.state === 'idle') {
        if (self.scanner.list.length === 0) {
          return;
        }

        $('#market-scanner-item-slider').attr('max', self.scanner.list.length);
        self.tree.jstree('lock');

        self.scanner.timer = setInterval(function () {
          self.onUpdate();
        }, 100);

        $('#market-scanner-start').attr('value', 'Pause');
        self.scanner.state = 'running';

      } else if (self.scanner.state === 'paused') {
        $('#market-scanner-start').attr('value', 'Pause');
        $('#market-scanner-reset').hide();
        $('#market-scanner-back').hide();
        $('#market-scanner-forward').hide();
        $('#market-scanner-item-slider').hide();

        self.scanner.lastUpdate = new Date().getTime();
        self.scanner.state = 'running';

      } else if (self.scanner.state === 'running') {
        $('#market-scanner-start').attr('value', 'Resume');
        $('#market-scanner-reset').show();
        $('#market-scanner-back').show();
        $('#market-scanner-forward').show();
        $('#market-scanner-item-slider').show();
        self.scanner.state = 'paused';
      }
    });

    // On clicking reset
    $('#market-scanner-reset').click(function () {
      self.reset();
    });

    // On clicking back or forward
    $('#market-scanner-back').click(function () {
      if (self.scanner.index > 0) {
        self.scanner.timeSinceLastUpdate = self.scanner.interval;
        self.scanner.index--;
        self.updateProgress();
      }
    });

    $('#market-scanner-forward').click(function () {
      var length = self.scanner.list.length;

      if (self.scanner.index < length) {
        self.scanner.timeSinceLastUpdate = self.scanner.interval;
        self.scanner.index++;
        self.updateProgress();
      }
    });

    $('#market-scanner-item-slider').change(function () {
      self.scanner.index = parseInt($(this).val(), 10);
      self.scanner.timeSinceLastUpdate = self.scanner.interval;
      self.updateProgress();
    });

    // On clicking 'uncheck all'
    $('#market-scanner-uncheck-all').click(function () {
      self.tree.jstree('uncheck_all');
      triggerItemCount();
    });

    // On clicking 'save selection'
    self.savedSelections = {};
    $('#market-scanner-save-preset').click(function () {
      var name      = $('#market-scanner-preset-name').val(),
          selected  = $('#market-scanner-preset-dropdown option:selected'),
          selection = self.getSelection();

      if (name && selection.length > 0) {
        $('#market-scanner-save-preset').attr('disabled', 'disabled');

        $.ajax({
          type: 'POST',
          dataType: 'json',
          url: '/presets/create',
          data: {
            name: name,
            selection: JSON.stringify(selection)
          }
        }).success(function (json) {
          var id = json.id;
          var option = $('<option/>').val(id).text(name).attr('selected', 'selected');

          self.savedSelections[id] = selection;
          selected.removeAttr('selected');
          $('#market-scanner-preset-dropdown').append(option);
          $('#market-scanner-preset-name').val('');

        }).complete(function () {
          $('#market-scanner-save-preset').removeAttr('disabled');
        });
      }
    });

    // On clicking 'apply'
    $('#market-scanner-apply-preset').click(function () {
      var selected   = $('#market-scanner-preset-dropdown option:selected'),
          id         = parseInt(selected.val(), 10),
          selection  = self.savedSelections[id];

      if (selected.length) {
        self.setSelection(selection);
      }
    });

    // On clicking 'delete'
    $('#market-scanner-delete-preset').click(function () {
      var selected = $('#market-scanner-preset-dropdown option:selected'),
          id       = selected.val();

      if (selected.length) {
        delete self.savedSelections[id];
        selected.remove();
        $.ajax({type: 'DELETE', url: '/presets/' + id});
      }
    });

    // On number of items checked changed
    $(this).bind('item_count_changed', function (e, count) {
      $('#itemCount').text(count);

      self.scanner.list = self.getSelectedItems();
      self.updateProgress();
    });

    // On node checked/unchecked
    self.tree.bind('check_node.jstree', triggerItemCount);
    self.tree.bind('uncheck_node.jstree', triggerItemCount);

    // Initialize all labels and controls
    this.reset();
    this.updateProgress();
    $('#market-scanner-interval-label').text(($('#market-scanner-interval').attr('value') / 10) + 's');

    // Load preset dropdown values
    $.getJSON('/presets').success(function (presets) {
      presets.forEach(function (preset) {
        var option  = $('<option/>').val(preset.id).text(preset.name);

        $('#market-scanner-preset-dropdown').append(option);
        self.savedSelections[preset.id] = preset.selection;
      });
    });
  };

  // Returns the given seconds in a HH:MM:SS format
  window.MarketScanner.prototype.formatHMS = function (seconds) {
    var hours   = Math.floor(seconds / 3600),
        minutes = Math.floor((seconds % 3600) / 60);
    seconds = Math.floor((seconds % 3600) % 60);

    return (hours < 10 ? '0' + hours : hours) + ':' + (minutes < 10 ? '0' + minutes : minutes) + ':' + (seconds < 10 ? '0' + seconds : seconds);
  };

  window.MarketScanner.prototype.onUpdate = function () {
    if (this.scanner.state === 'running') {
      var now      = new Date().getTime(),
          typeID   = this.scanner.list[this.scanner.index].typeID;

      this.scanner.timeSinceLastUpdate += now - this.scanner.lastUpdate;
      this.scanner.lastUpdate = now;

      if (this.scanner.timeSinceLastUpdate >= this.scanner.interval) {
        // Have we scanned all items?
        if (this.scanner.index >= this.scanner.list.length) {
          this.reset();
          return;
        }

        if (typeof CCPEVE !== 'undefined') {
          CCPEVE.showMarketDetails(typeID);
        }

        this.scanner.index += 1;
        this.scanner.timeSinceLastUpdate = 0;
        this.updateProgress();
      }
    }
  };

  window.MarketScanner.prototype.reset = function () {
    $(this.tree).jstree('unlock');

    if (typeof this.scanner.timer !== 'undefined') {
      clearInterval(this.scanner.timer);
    }

    this.scanner.index = 0;
    this.scanner.timeSinceLastUpdate = this.scanner.interval;
    this.scanner.lastUpdate = new Date().getTime();
    this.scanner.state = 'idle';

    $('#market-scanner-reset').hide();
    $('#market-scanner-back').hide();
    $('#market-scanner-forward').hide();
    $('#market-scanner-item-slider').hide();
    $('#market-scanner-start').attr('value', 'Start');

    this.updateProgress();
  };

  window.MarketScanner.prototype.updateProgress = function () {
    var
      numItems    = parseInt($('#itemCount').text(), 10),
      itemsLeft   = numItems - this.scanner.index + 1,
      secondsLeft = (this.scanner.interval * itemsLeft - this.scanner.timeSinceLastUpdate) / 1000.0,
      text        = this.scanner.index < this.scanner.list.length ? this.scanner.list[this.scanner.index].typeName : '';

    $('#market-scanner-next-item').text(text);
    $('#market-scanner-progress').text(this.scanner.index);
    $('#market-scanner-timeleft').text(this.formatHMS(secondsLeft));
  };

  // The tree selection contains a list of marketgroup or typeid li's, we need to convert this
  // into a list of objects: [{typeID: typeID, typeName: typeName, marketGroupID: marketGroupID}], the selected items.
  window.MarketScanner.prototype.getSelectedItems = function () {
    var
      items,
      marketGroupItems,
      self              = this,
      checked           = this.tree.jstree('get_checked'),
      itemNodes         = checked.filter(function () { return $(this).attr('typeid') !== undefined; }),
      marketGroupNodes  = checked.filter(function () { return $(this).attr('typeid') === undefined; });

    // All manually selected items
    items = itemNodes.map(function () {
      var typeID        = parseInt($(this).attr('typeid'), 10),
          typeName      = $.trim($(this).children('a').text()),
          marketGroupID = parseInt($(this).parents('li:first').attr('marketgroupid'), 10);

      return {typeID: typeID, typeName: typeName, marketGroupID: marketGroupID};
    });

    // All items belonging to the selected market groups
    marketGroupItems = marketGroupNodes.map(function () {
      var marketGroupID = parseInt($(this).attr('marketgroupid'), 10);
      return self.getMarketGroupItems(self.getMarketGroupData(marketGroupID));
    });

    return $.merge(items, marketGroupItems);
  };

  // Returns the items [{typeID: typeID, typeName: typeName, marketGroupID: marketGroupID}] of a market group by traversing it down recursively.
  window.MarketScanner.prototype.getMarketGroupItems = function (marketGroup, array) {
    var i, element, marketGroupID, typeID, typeName;

    if (typeof array === 'undefined') {
      array = [];
    }

    for (i = 0; i < marketGroup.children.length; i++) {
      element       = marketGroup.children[i];
      marketGroupID = element.attr.marketGroupID;
      typeID        = element.attr.typeID;
      typeName      = element.data;

      if (typeof typeID !== 'undefined') {
        array.push({typeID: parseInt(typeID, 10), typeName: typeName, marketGroupID: marketGroupID});

      } else {
        // Continue down that group
        this.getMarketGroupItems(element, array);
      }
    }

    return array;
  };

  // Returns the data node corresponding to the given marketGroupID
  // It uses the marketGroupID -> parentMarketGroupID table to avoid having to search through the whole table.
  window.MarketScanner.prototype.getMarketGroupData = function (marketGroupID) {
    var
      i,
      element,
      parentID       = this.parent_lookup[marketGroupID],
      searchLocation = typeof parentID === 'undefined' ? this.market_data : this.getMarketGroupData(parentID).children;

    for (i = 0; i < searchLocation.length; i++) {
      element = searchLocation[i];

      // It will always be exactly one
      if (element.attr.marketGroupID === marketGroupID) {
        return element;
      }
    }
  };

  // Returns the current selection as a list of
  // {type: 'item', marketGroupID: marketGroupID, typeID: typeID} and {type: 'group', marketGroupID: marketGroupID},.
  window.MarketScanner.prototype.getSelection = function () {
    return this.tree.jstree('get_checked').map(function () {
      var element       = $(this),
          type          = element.attr('type'),
          marketGroupID = element.attr('marketgroupid'),
          typeID        = element.attr('typeid'),
          result        = {type: type, marketGroupID: marketGroupID };

      if (type === 'item') {
        result.typeID = typeID;
      }

      return result;
    }).toArray();
  };

  // Returns the path from the root down to the given marketGroupID as an array
  window.MarketScanner.prototype.getPath = function (marketGroupID) {
    var array = [marketGroupID];

    while (this.parent_lookup[marketGroupID] !== undefined) {
      marketGroupID = this.parent_lookup[marketGroupID];
      array.push(marketGroupID);
    }

    return array.reverse();
  };

  // See getSelection() for what kind of input this function excepts
  // TODO: Can be optimized, only market groups that has selected children needs to be loaded.
  window.MarketScanner.prototype.setSelection = function (selection) {
    var marketGroupPaths,
        added   = {},
        groups  = [],
        self    = this;

    // We need an array of all market groups in all paths but without duplicates
    marketGroupPaths = selection.map(function (element) {
      return self.getPath(element.marketGroupID);
    });

    marketGroupPaths.forEach(function (element) {
      var level, marketGroupID;

      for (level = 0; level < element.length; level++) {
        marketGroupID = element[level];

        if (!added[marketGroupID]) {
          added[marketGroupID] = true;
          groups.push({id: marketGroupID, level: level});
        }
      }
    });

    // Sort by top level first so that we load the tree in the correct order
    groups.sort(function (a, b) { return a.level - b.level; });

    // Force all required nodes to load
    groups.forEach(function (group) {
      self.tree.jstree('open_node', 'li[type=group][marketgroupid=' + group.id + ']');
    });

    self.tree.jstree('uncheck_all');

    selection.forEach(function (element) {
      var selector;

      if (element.type === 'group') {
        selector = 'li[type=group][marketgroupid=' + element.marketGroupID + ']';

      } else {
        selector = 'li[type=item][typeid=' + element.typeID + ']';
      }

      self.tree.jstree('check_node', selector);
    });

    self.tree.jstree('close_all');
  };
})(jQuery);
