(function ($) {
  'use strict';

  var createTable = function (columns) {
    var table  = $('<table/>').attr('class', 'table table-condensed table-striped table-bordered'),
        trHead = $('<tr/>').appendTo($('<thead/>').appendTo(table));

    columns.forEach(function (header) {
      trHead.append($('<th/>').text(header));
    });

    $('<tbody/>').appendTo(table);

    return table;
  };

  var createDataTable = function (table, aaAjaxSource, aaSorting, fnServerData, options) {

    var adjustTable = function (dataTable) {
      dataTable.fnAdjustColumnSizing(false);

      // Make table fill rest of page (if enabled)
      if (typeof options !== 'undefined' && options.bAutoFill) {
        var scrollBody    = dataTable.parents().find('.dataTables_scrollBody:first'),
            verticalSpace = $(window).height() - scrollBody.offset().top - 29;
        scrollBody.height(verticalSpace);
      }
    };

    return table.dataTable($.extend({
      'bPaginate': false,
      'sScrollY': '300px',
      'bScrollCollapse': true,
      'bProcessing': true,
      'bServerSide': true,
      'sAjaxSource': aaAjaxSource,
      'aaSorting': aaSorting,
      'sDom': 'prt',
      'fnServerData': function (sSource, aoData, fnCallback) {
        var dataTable = this;
        var callback  = function () {
          // Automatically readjust table after updating data
          fnCallback.apply(this, arguments);
          adjustTable(dataTable);
        };

        if (!fnServerData.call(this, sSource, aoData, callback)) {
          // Find sEcho (should be at index 0, but that can't be guaranteed)
          var sEcho, i;
          for (i = 0; i < aoData.length; i++) {
            if (aoData[i].name === 'sEcho') {
              sEcho = aoData[i].value;
              break;
            }
          }

          // Pretend no data was returned from the server
          fnCallback({
            sEcho: sEcho,
            aaData: [],
            iTotalRecords: 0,
            iTotalDisplayRecords: 0
          });
        }
      }
    }, options || {})).addClass('get-datatable')
    .on('visible', function () {
      adjustTable($(this).dataTable());
    });
  };

  var createTypeahead = function (loaded_event, default_value, selector) {
    var typeahead = (selector ? $(selector) : $('<input/>').attr('type', 'text'))
      .attr('data-provide', 'typeahead')
      .attr('class', 'typeahead')
      .attr('placeholder', 'Type to search')
      .typeahead();

    $(document).on(loaded_event, function (e, items) {
      typeahead.data('typeahead').source = items.map(function (item) { return item.name; });

      // Create reverse lookup name -> id since the bootstrap typeahead item can't hold metadata
      typeahead.reverse_lookup = {};
      items.forEach(function (item) {
        typeahead.reverse_lookup[item.name] = item.id;
      });

      if (default_value) {
        typeahead.val(default_value);
        typeahead.change();
      }
    });

    return typeahead;
  };

  var market_view_index = 0;

  // Creates a market view (current buy and sell orders + history) in the given container element
  $.fn.market_view = function () {
    if (this.length === 0) {
      return;
    }

    market_view_index++;

    var createMarketViewDataTable = function (table, type, aaAjaxSource, aaSorting, options) {
      return createDataTable(table, aaAjaxSource, aaSorting, function (sSource, aoData, fnCallback) {
        var regionID = get_region_id(),
            typeID = get_type_id();

        if (regionID && typeID) {
          sSource += '/' + regionID + '/' + typeID + (typeof type !== 'undefined' ? '/' + type : '');

          $.getJSON(sSource, aoData, function (json) {
            fnCallback(json);
          });

          return true;

        } else {
          return false;
        }
      }, options);
    };

    // Create the two tabs and tab-bar
    var market_data_tab_id   = 'market-view-' + market_view_index + '-data-tab';
    var price_history_tab_id = 'market-view-' + market_view_index + '-market-history-tab';

    var tab_bar = $('<ul/>').attr('class', 'nav nav-tabs').attr('data-tabs', 'tabs');
    tab_bar.append(
      $('<li/>')
        .attr('class', 'active')
        .append(
          $('<a/>')
            .attr('data-toggle', 'tab')
            .attr('href', '#' + market_data_tab_id).text('Market data')
        )
    );

    tab_bar.append(
      $('<li/>').append(
        $('<a/>')
          .attr('href', '#' + price_history_tab_id)
          .attr('data-toggle', 'tab').text('Price history')
      )
    );

    var market_data_tab = $('<div/>').attr('id', market_data_tab_id).attr('class', 'tab-pane active');
    var history_tab = $('<div/>').attr('id', price_history_tab_id).attr('class', 'tab-pane');

    var tab_content = $('<div/>').attr('class', 'tab-content');
    tab_content.append(market_data_tab);
    tab_content.append(history_tab);

    // Create the region and item form
    var region_select = $('<select/>');
    var item_input = createTypeahead('items_loaded');

    $(document).on('regions_loaded', function (e, items) {
      items.forEach(function (item) {
        $('<option/>').val(item.id).text(item.name).appendTo(region_select);
      });

      // Set default value
      region_select.find('option:contains("The Forge")').attr('selected', 'selected');
    });

    var get_region_id = function () {
      return region_select.find('option:selected').val();
    };

    var get_type_id = function () {
      if (typeof item_input.reverse_lookup !== 'undefined') {
        return item_input.reverse_lookup[item_input.val()];
      }
    };

    this
      .append($('<div/>').append(region_select))
      .append($('<div/>').append(item_input))
      .append(
        $('<div/>')
          .append(tab_bar)
          .append(tab_content)
      );

    var sellTable    = createMarketViewDataTable(createTable(['Quantity', 'Price', 'Location', 'Expires in']).appendTo(market_data_tab), 'sell', '/orders', [[1, 'asc']]),
        buyTable     = createMarketViewDataTable(createTable(['Quantity', 'Price', 'Location', 'Range', 'Min volume', 'Expires in']).appendTo(market_data_tab), 'buy', '/orders', [[1, 'desc']]),
        historyTable = createMarketViewDataTable(createTable(['Date', 'Orders', 'Quantity', 'Low', 'High', 'Average']).appendTo(history_tab), undefined, '/history', [[0, 'desc']], {'sScrollY': '100%'});

    // Update tables on input changes
    var updateTables = function () {
      sellTable.fnDraw();
      buyTable.fnDraw();
      historyTable.fnDraw();
    };

    region_select.change(updateTables);
    item_input.change(updateTables);

    var adjustScrollHeights = function () {
      // Set scroll heights making both table equal height and filling the whole document
      var upperScroll   = sellTable.parents().find('.dataTables_scrollBody:first'),
          lowerScroll   = buyTable.parents().find('.dataTables_scrollBody:first'),
          verticalSpace = $(window).height() - upperScroll.offset().top - 29 * 2;
      upperScroll.height(verticalSpace / 2.0);
      lowerScroll.height(verticalSpace / 2.0);
    };

    sellTable.on('visible', adjustScrollHeights);
    buyTable.on('visible', adjustScrollHeights);
  };

  $.fn.item_breakdown_table = function () {
    if (this.length === 0) {
      return;
    }

    var table = createTable([
      'Item', 'Profit', 'Margin', 'ISK volume', 'Volume', 'Sell price', 'Buy price', 'Average price',
      'Sell price relative average', 'Buy price relative average', 'Supply vs Demand (market)', 'Supply vs Demand (history)', 'Competition'
    ]).appendTo(this);

    var solar_system_input = createTypeahead('solar_systems_loaded', 'Jita', '#solar-system');
    var get_solar_system_id = function () {
      if (typeof solar_system_input.reverse_lookup !== 'undefined') {
        return solar_system_input.reverse_lookup[solar_system_input.val()];
      }
    };

    var get_history_date = function () {
      var daysBack = $('#history-select option:selected').val(),
          now = new Date();

      if (daysBack !== 'all') {
        now.setDate(now.getDate() - parseInt(daysBack, 10));
        return now.getUTCFullYear() + '-' + ('0' + (now.getUTCMonth() + 1)).slice(-2) + '-' + ('0' + now.getUTCDate()).slice(-2);
      }
    };

    this.append(table);
    var dataTable = createDataTable(table, '/item_breakdown', [[1, 'desc']], function (sSource, aoData, fnCallback) {
      var solar_system_id = get_solar_system_id();

      if (solar_system_id) {
        sSource += '/' + solar_system_id.region_id;
        aoData.push({name: 'solar_system_id', value: solar_system_id.solar_system_id});

        // Add filters arguments
        var filter_variables = {
          '#isk-min': 'isk_volume_min',
          '#isk-max': 'isk_volume_max',
          '#margin-min': 'margin_min',
          '#margin-max': 'margin_max',
          '#sell-price-min': 'sell_price_min',
          '#sell-price-max': 'sell_price_max',
          '#buy-price-min': 'buy_price_min',
          '#buy-price-max': 'buy_price_max',
          '#volume-min': 'volume_min',
          '#volume-max': 'volume_max'
        };

        for (var key in filter_variables) {
          var value          = $(key).get(0)._val,
              parameter_name = filter_variables[key];

          if (value) {
             // Convert to decimal
            if (parameter_name === 'margin_min' || parameter_name === 'margin_max') {
              value = value / 100.0 + 1.0;
            }

            aoData.push({ name: parameter_name, value: value });
          }
        }

        // Add history age filter
        var history_date = get_history_date();
        if (history_date) {
          aoData.push({ name: 'history_after', value: history_date });
        }

        $.getJSON(sSource, aoData, function (json) {
          fnCallback(json);
        });

        return true;

      } else {
        return false;
      }
    }, {'bPaginate': true, 'iDisplayLength': 100, 'bAutoFill': true});

    solar_system_input.change(function () {
      dataTable.fnDraw();
    });

    var number_with_thousand_grouping = function (str) {
      return str.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    };

    // Auto formatting of filter inputs
    $('.filter.margin').focusin(function () {
      $(this).val($(this).get(0)._val || '');
    });

    $('.filter.margin').focusout(function () {
      var value = $(this).val(),
          floatValue = parseFloat(value);
      this._val = value;

      if (value !== '') {
        $(this).val(number_with_thousand_grouping(floatValue.toFixed(2)) + '%');
      }
    });

    $('.filter.isk').focusout(function () {
      var value = $(this).val(),
          floatValue = parseFloat(value),
          suffix = '';

      this._val = value;

      if (value !== '') {

        if (floatValue > 1e9) {
          floatValue = floatValue / 1e9;
          suffix = ' B';
        }
        else if (floatValue > 1e6) {
          floatValue = floatValue / 1e6;
          suffix = ' M';
        }

        $(this).val(number_with_thousand_grouping(floatValue.toFixed(2)) + suffix);
      }
    });

    $('.filter.isk').focusin(function () {
      $(this).val($(this).get(0)._val || '');
    });

    $('.filter').keypress(function (e) {
      // Only allow 0-9 and .
      var valid = (e.keyCode >= 48 && e.keyCode <= 57) || e.keyCode === 46;
      if (!valid) {
        e.preventDefault();
      }
    });

    $('#button-filter').click(function () {
      dataTable.fnDraw();
    });
  };

  $.fn.item_profit_breakdown_table = function () {
    if (this.length === 0) {
      return;
    }

    var table = createTable(['Item', 'Profit', 'Average margin', 'Average sell price', 'Average buy price', 'Sold', 'Bought', 'Unsold']);
    this.append(table);
    var dataTable = createDataTable(table, '/transactions/profit_by_item', [[1, 'desc']], function (sSource, aoData, fnCallback) {
      var transaction_start_date = $('#input-start-date').val(),
          transaction_end_date = $('#input-end-date').val();

      if (transaction_start_date) {
        aoData.push({name: 'transactions_start', value: transaction_start_date});
      }

      if (transaction_end_date) {
        aoData.push({name: 'transactions_end', value: transaction_end_date});
      }

      $.getJSON(sSource, aoData, function (json) {
        // Extract some extra information received
        $('#earliest-date').text(json.earliest_transaction);
        $('#latest-date').text(json.latest_transaction);
        $('#total-profit').text(json.total_profit);
        $('#monthly-profit').text(json.profit_per_month);

        fnCallback(json);
      });

      return true;
    }, {'bAutoFill': true});

    $('#transaction-date-form input[type="button"]').click(function () {
      dataTable.fnDraw();
    });
  };

  // Trigger datatable visible when then its tab is clicked
  $(document).on('shown', 'a[data-toggle="tab"]', function () {
    var tab_content_id = $(this).attr('href');

    $(tab_content_id).find('.get-datatable').each(function () {
      if ($(this).is(':visible')) {
        $(this).dataTable().trigger('visible');
      }
    });
  });

  $(function () {
    $('#market').market_view();
    $('#item-breakdown').item_breakdown_table();
    $('#transactions').item_profit_breakdown_table();

    $.getJSON('/regions', function (json) {
      $(document).trigger('regions_loaded', [json]);
    });

    $.getJSON('/solar_systems', function (json) {
      $(document).trigger('solar_systems_loaded', [json]);
    });

    $.getJSON('/items', function (json) {
      $(document).trigger('items_loaded', [json]);
    });
  });
})(jQuery);
