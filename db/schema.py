import sqlite3
import sys

def create_static_data(db_filename):
  conn = sqlite3.connect(db_filename)
  c = conn.cursor()

  c.execute('DROP TABLE IF EXISTS invTypes')
  c.execute('DROP TABLE IF EXISTS invMarketGroups')
  c.execute('DROP TABLE IF EXISTS mapDenormalize')

  # Import static EVE data
  for script in ['invTypes', 'invMarketGroups', 'mapDenormalize']:
    name = 'db/{}.sql'.format(script)
    conn.executescript(open(name).read())
    print 'Loaded', name

  # Create indices
  c.execute("CREATE INDEX IF NOT EXISTS invMarketGroups_parentGroupID ON invMarketGroups(parentGroupID);")
  c.execute("CREATE INDEX IF NOT EXISTS invTypes_marketGroupID ON invTypes(marketGroupID);")
  c.execute("CREATE INDEX IF NOT EXISTS invTypes_typeID ON invTypes(typeID);")
  c.execute("CREATE INDEX IF NOT EXISTS mapDenormalize_typeID ON mapDenormalize(typeID);")
  c.execute("CREATE INDEX IF NOT EXISTS mapDenormalize_solarSystemID ON mapDenormalize(solarSystemID);")
  c.execute("CREATE INDEX IF NOT EXISTS mapDenormalize_constellationID ON mapDenormalize(constellationID);")
  c.execute("CREATE INDEX IF NOT EXISTS mapDenormalize_regionID ON mapDenormalize(regionID);")

  conn.commit()
  conn.close()

def create(db_filename):
  conn = sqlite3.connect(db_filename)
  c = conn.cursor()

  c.execute('DROP TABLE IF EXISTS market_orders')
  c.execute('DROP TABLE IF EXISTS history')
  c.execute('DROP TABLE IF EXISTS presets')

  c.execute('''
    CREATE TABLE market_orders (
      orderID       INTEGER     PRIMARY KEY,
      issueDate     TIMESTAMP,
      price         REAL,
      volRemaining  INTEGER,
      minVolume     INTEGER,
      range         INTEGER,
      volEntered    INTEGER,
      bid           INTEGER,
      duration      INTEGER,
      jumps         INTEGER,
      typeID        INTEGER     REFERENCES invTypes(typeID),
      stationID     INTEGER     REFERENCES mapDenormalize(itemID),
      solarSystemID INTEGER     REFERENCES mapDenormalize(itemID),
      regionID      INTEGER     REFERENCES mapDenormalize(itemID)
    )
  ''')

  c.execute('''
    CREATE TABLE history (
      typeID        INTEGER     REFERENCES items(id),
      regionID      INTEGER     REFERENCES mapDenormalize(itemID),
      historyDate   TIMESTAMP,
      lowPrice      REAL,
      highPrice     REAL,
      avgPrice      REAL,
      volume        INTEGER,
      orders        INTEGER,
      PRIMARY KEY(regionID, typeID, historyDate)
    )
  ''')

  c.execute('''
    CREATE TABLE presets (
      id         INTEGER PRIMARY KEY,
      name       TEXT,
      selection  TEXT
    )
  ''')

  c.execute('''
    CREATE TABLE transactions (
      transactionID         INTEGER PRIMARY KEY,
      transactionType       TEXT,
      transactionDateTime   TIMESTAMP,
      journalTransactionID  INTEGER,
      transactionFor        TEXT,
      price                 REAL,
      quantity              INTEGER,
      typeID                INTEGER,
      typeName              TEXT,
      clientID              INTEGER,
      clientName            TEXT,
      stationID             INTEGER,
      stationName           TEXT
    )
  ''')

  # Create indices
  c.execute("CREATE INDEX IF NOT EXISTS market_orders_typeID ON market_orders(typeID)")
  c.execute("CREATE INDEX IF NOT EXISTS market_orders_stationID ON market_orders(stationID)")
  c.execute("CREATE INDEX IF NOT EXISTS market_orders_solarSystemID ON market_orders(solarSystemID)")
  c.execute("CREATE INDEX IF NOT EXISTS market_orders_regionID_and_typeID ON market_orders(regionID, typeID)")
  c.execute("CREATE INDEX IF NOT EXISTS market_orders_regionID_and_solarSystemID_and_bid_and_typeID ON market_orders(regionID, solarSystemID, bid, typeID)")

  c.execute("CREATE INDEX IF NOT EXISTS history_typeID ON history(typeID)")
  c.execute("CREATE INDEX IF NOT EXISTS history_regionID ON history(regionID)")
  c.execute("CREATE INDEX IF NOT EXISTS history_historyDate ON history(historyDate)")

  c.execute("CREATE INDEX IF NOT EXISTS transactions_typeName ON transactions(typeName)")
  c.execute("CREATE INDEX IF NOT EXISTS transactions_transactionType_and_typeName ON transactions(transactionType, typeName)")

  conn.commit()
  conn.close()

if __name__ == '__main__':
  db_name = 'db.sqlite3'

  if len(sys.argv) > 1 and sys.argv[1] in ['all', 'static', 'non-static']:
    arg = sys.argv[1]

    message = {
      'all': 'This wipes all data, both static and non-static data',
      'static': 'This wipes all static data',
      'non-static': 'This wipes all non-static data',
    }

    sys.stdout.write('{} from {}, are you sure? '.format(message[arg], db_name))
    response = raw_input()

    if response.lower() in ['y', 'yes']:
      if arg == 'all' or arg == 'non-static':
        print 'Creating non-static tables...'
        create(db_name)
        print 'Done!'

      if arg == 'all' or arg == 'static':
        print 'Creating and filling static EVE data tables...'
        create_static_data(db_name)
        print 'Done!'
      

    else:
      print 'Aborting...'

  else:
    print 'Usage: you need to specify all, static or non-static as the first argument'