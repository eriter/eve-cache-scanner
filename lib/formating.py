from fractions import Fraction

# We want to get a as simple (low number) fractions as possible.
# The problem is that for small numbers fractions.Fraction() outputs 0/1, so we need to calculate
# the minimum number of decimal digits to include to get a non 0/1 output.
def simple_fraction(value):
  if value == 0: return '0:1'

  base_num_decimals = 1
  highest_ok_value = 1.0 / (10 ** base_num_decimals)

  if value < highest_ok_value:
    num_leading_decimal_zeros = int('{:e}'.format(value).split('e-')[1])
    num_decimals = num_leading_decimal_zeros
  else:
    num_decimals = base_num_decimals

  fraction = Fraction(('{:.' + str(num_decimals) + 'f}').format(value))
  return '{:d}:{:d}'.format(fraction.numerator, fraction.denominator)

def format_number(number):
  # Apply prefix and always include two decimals
  if isinstance(number, float):
    if number > 1e9:
      return '{:,.2f} B'.format(number / 1e9)
    elif number > 1e6:
      return '{:,.2f} M'.format(number / 1e6)
    else:
      return '{:,.2f}'.format(number)
  else:
    # Just add thousand separator
    return '{:,d}'.format(number)

def elapsed_time(seconds):
  r = seconds
  days, r = divmod(r, 24*60*60)
  hours, r = divmod(r, 60*60)
  minutes, r = divmod(r, 60)
  seconds = r

  result = []
  if days > 0: result.append('%dd' % days)
  if hours > 0: result.append('%dh' % hours)
  if minutes > 0: result.append('%dm' % minutes)
  if seconds > 0: result.append('%ds' % seconds)

  return ' '.join(result)