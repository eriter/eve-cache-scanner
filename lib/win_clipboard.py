import ctypes

# This uses the windows API so we need to check if it's available

if hasattr(ctypes, 'windll'):
  CLIPBOARD_AVAILABLE = True

  # Get required functions
  strcpy            = ctypes.cdll.msvcrt.strcpy
  OpenClipboard     = ctypes.windll.user32.OpenClipboard
  EmptyClipboard    = ctypes.windll.user32.EmptyClipboard
  GetClipboardData  = ctypes.windll.user32.GetClipboardData
  SetClipboardData  = ctypes.windll.user32.SetClipboardData
  CloseClipboard    = ctypes.windll.user32.CloseClipboard
  GlobalAlloc       = ctypes.windll.kernel32.GlobalAlloc    # Global Memory allocation
  GlobalLock        = ctypes.windll.kernel32.GlobalLock     # Global Memory Locking
  GlobalUnlock      = ctypes.windll.kernel32.GlobalUnlock
  GMEM_DDESHARE     = 0x2000 

  def read_clipboard():
    OpenClipboard(None)

    # Read contents and convert to string
    p_contents = GetClipboardData(1) # 1 means CF_TEXT
    data = ctypes.c_char_p(p_contents).value

    CloseClipboard()

    return data

  def write_clipboard(data):
    OpenClipboard(None)
    EmptyClipboard()

    hCd = GlobalAlloc(GMEM_DDESHARE, len(str(data)) + 1)
    pchData = GlobalLock(hCd)
    strcpy(ctypes.c_char_p(pchData), str(data))

    GlobalUnlock(hCd)
    SetClipboardData(1, hCd)
    CloseClipboard()
else:
  CLIPBOARD_AVAILABLE = False

  def read_clipboard():
    raise Exception('Clipboard only available on Windows.')

  def write_clipboard(value):
    raise Exception('Clipboard only available on Windows.')