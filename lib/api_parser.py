''' Simple parser for a xml response from the EVE api server. '''
import xml.etree.ElementTree as ET
from datetime import datetime

class Parser(object):

  def parse(self, xml):
    ''' Parses the given EVE api xml string to a json-like structure. '''
    root   = ET.fromstring(xml)
    error  = root.find('error')
    result = root.find('result')
    parse_date = lambda x: datetime.strptime(x, '%Y-%m-%d %H:%M:%S')

    return {
      'currentTime': parse_date(root.find('currentTime').text),
      'cachedUntil': parse_date(root.find('cachedUntil').text),
      'error': error.text if error is not None else None,
      'result': self.to_dict(result) if result is not None else None,
    }

  def to_dict(self, element):
    ''' Converts the given xml node to a json-like list/dict object. '''
    d = {}
    if element.text is not None and element.text.strip() != '':
      return element.text
    else:
      for child in element:
        if child.tag == 'rowset':
          key = child.attrib['name']
          value = [self.to_dict(row) for row in child]
        else:
          key = child.tag
          value = self.to_dict(child)

        d[key] = value

    # Add attributes
    d.update((k, v) for k, v in element.attrib.iteritems())
    return d