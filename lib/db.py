
def insert_rows(conn, table_name, rows):
  ''' Rows can be either Reverence's blue.DBRow or a simple list of dicts. '''

  c = conn.cursor()
  count = 0

  for row in rows:
    # Count manually as Cursor.rowcount is unreliable
    count += 1

    # blue.DBRow has __keys__, dict has keys()
    keys = hasattr(row, '__keys__') and row.__keys__ or row.keys()
    column_names = ', '.join([':' + key for key in keys])

    # table_name and column_names are assumed safe
    c.execute("INSERT OR REPLACE INTO {} ({}) VALUES ({})".format(table_name, ', '.join(keys), column_names), row)

  conn.commit()
  c.close()
  return count