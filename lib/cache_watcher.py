''' 
Watches the EVE cache directory for created / modified files updating
the database with market orders and history.
'''
import logging
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
from time import time

from lib.db import insert_rows
from lib.cache_parser import Parser
from lib.win_clipboard import write_clipboard, CLIPBOARD_AVAILABLE

class EveCacheObserver(FileSystemEventHandler):
  ''' Responds to file events using watchdog. '''

  def __init__(self, connect, eve_installation_path):
    self._connect = connect
    self._parser = Parser(eve_installation_path)
    self._observer = Observer()
    self._watch_directory = self._parser.get_cached_method_calls_path()
    self._observer.schedule(self, self._watch_directory, recursive=True)
    self._conn = None
    self._market_update_mode = 'off'

    # dict of filenames and a timestamp of when their were last parsed
    self._last_parsed = {}

  def start(self):
    ''' Starts the file watcher in a separate thread. '''
    self._observer.start()
    logging.info("Watching directory '%s'", self._watch_directory)

  def stop(self):
    ''' Stops the file watcher. '''
    self._observer.stop()
    self._observer.join()

  def set_market_update_mode(self, value):
    assert value in set(['buy', 'sell', 'off'])
    self._market_update_mode = value
    logging.info('Set market update mode to "%s".', value)

  def on_modified(self, event):
    self.parse(event.src_path)

  def on_created(self, event):
    self.parse(event.src_path)

  def handle_market_orders(self, key, orders):
    ''' Inserts market orders into the database. '''
    self._conn = self._conn or self._connect()

    region_id, type_id = key[2:]
    orders = list(orders)

    count = insert_rows(self._conn, 'market_orders', orders)
    logging.info("Read/updated %d market orders for %d in %d", 
      count, type_id, region_id)

    # Since this cached method contains an accurate picture of all currently
    # existing orders remove all orders not updated above
    existing_ids = ', '.join([str(row['orderID']) for row in orders])

    c = self._conn.cursor()
    c.execute(
      "DELETE FROM market_orders " \
      "WHERE regionID = ? AND typeID = ? AND NOT orderID IN ({})"
      .format(existing_ids), (region_id, type_id))
    self._conn.commit()

    if c.rowcount > 0:
      logging.info(
        "Deleted %d market orders for %d in %d that no longer exists.",
        c.rowcount,
        type_id,
        region_id
      )

    # Copy new price (under/over-cutted) to clipboard
    if CLIPBOARD_AVAILABLE and self._market_update_mode != 'off':
      is_buy = self._market_update_mode == 'buy'

      station_filters = {
        10000002: 60003760, # Jita -> Jita IV - Moon 4 - Caldari Navy Assembly Plant
        10000043: 60008494, # Domain -> Amarr VIII (Oris) - Emperor Family Academy
      }

      station_filter = lambda order: region_id not in station_filters or order['stationID'] == station_filters[region_id]
      prices = [order['price'] for order in orders if is_buy and order['bid'] == 1 or not is_buy and order['bid'] == 0 and station_filter(order)]

      if len(prices) > 0:
        old_price = (max if is_buy else min)(prices)
        new_price = old_price + (0.01 if is_buy else -0.01)

        logging.info('Copying new %s price %.2f to clipboard', self._market_update_mode, new_price)
        write_clipboard(str('{:.2f}'.format(new_price)))

  def handle_old_market_history(self, key, history):
    ''' Inserts market history into the database. '''
    self._conn = self._conn or self._connect()

    region_id, type_id = key[2:]
    count = insert_rows(self._conn, 'history', history)
    logging.info(
      "Read/updated %d days of history for %d in %d",
      count, type_id, region_id
    )

  def parse(self, filename):
    ''' Parses the given cache file inserting any present
        market orders or history into the database. '''

    now = time()
    handlers = {
      'GetOrders': self.handle_market_orders,
      'GetOldPriceHistory': self.handle_old_market_history,
    }

    # Prevent the same cache file from being read multiple time 
    # (happends since we're listening to both modified and created file events)
    if (now - self._last_parsed.get(filename, 0)) < 0.5:
      return

    self._last_parsed[filename] = now

    try:
      cache_key, cache_content = self._parser.parse(filename)
      method_name = cache_key[1]

      if method_name in handlers:
        handlers[method_name](cache_key, cache_content)
      else:
        logging.info("Ignoring unknown cached method '%s'.", method_name)

    except IOError:
      logging.exception('Failed to parse cache file "{}".'.format(filename))

def main():
  ''' Runs the cache watcher independently form the rest of the application '''
  import sqlite3
  from time import sleep

  # Configure logging
  logging.basicConfig(
    level=logging.INFO,
    format='[%(levelname)s: %(asctime)s] %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S'
  )

  connect = lambda: sqlite3.connect(
    'db.sqlite3',
    detect_types=sqlite3.PARSE_DECLTYPES
  )

  observer = EveCacheObserver(connect, 'C:/Program Files (x86)/CCP/EVE')
  observer.start()

  try:
    while True:
      sleep(1)
  except KeyboardInterrupt:
    observer.stop()
  
if __name__ == '__main__':
  main()