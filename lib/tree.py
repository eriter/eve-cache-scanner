# Generates market dictionary for the jstree.

# 2012-12-22 Modified the input source and the output format but did not
# change anything of the actual code.
# TODO: Needs to be rewritten, can probably be improved a lot

def generate_tree(items, market_groups):
  '''
    items: List of tuples of format (typeID, typeName, marketGroupID)
    market_groups: List of tuples of format (marketGroupID, parentGroupID, marketGroupName)
  '''

  marketGroupDict = {}    # Handles to each market group
  market = []             # The full hierarchy
  
  parentDict = {}         # marketGroupID -> parentGroupID lookup
  lists = [market]        # List containing all lists, so they can be sorted

  for group in market_groups:
    marketGroupID, parentGroupID, marketGroupName = group
    children = []
    lists.append(children)
    
    # Create and add market group
    marketGroupDict[marketGroupID] = {
      'data'     : marketGroupName,
      'attr'     : { 'marketGroupID': marketGroupID, 'type': 'group'},
      'children' : children,
    }
    
    if parentGroupID is not None:
      parentDict[marketGroupID] = parentGroupID

  # Using the parentGroupID, build the market hierarchy
  for id,group in marketGroupDict.items():

    # Is this a base group?
    if id not in parentDict:
      market.append(group)
    else:
      # Find parent
      parent = marketGroupDict[parentDict[id]]
      parent['children'].append(group)
  
  # Fill market with items
  for item in items:
    typeID, typeName, marketGroupID = item
    
    group = marketGroupDict[marketGroupID]
    group['children'].append({
      'data': typeName,
      'attr': { 'typeID' : typeID, 'marketGroupID': marketGroupID, 'type': 'item' },
    })
    
    # Increase item count by one for this group, and all of it's parent groups
    incrementItemCount(group, marketGroupDict, parentDict)
    
  # Sort all lists by 'data'
  for item in lists:
    item.sort(cmp=lambda a,b: cmp(a['data'], b['data']))

  return {'tree': market, 'parents': parentDict}

def incrementItemCount(group, marketGroupDict, parentDict):
  if 'itemCount' not in group['attr']:
    group['attr']['itemCount'] = 0
      
  group['attr']['itemCount'] += 1
  
  # Increment this group's parent's item count
  marketGroupID = group['attr']['marketGroupID']
  if marketGroupID in parentDict:
    incrementItemCount(marketGroupDict[parentDict[marketGroupID]], marketGroupDict, parentDict)