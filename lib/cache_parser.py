''' 
Reads and parses EVE cache files using the Reverence cache parser.
See: https://github.com/ntt/reverence
'''
import itertools
import os
from datetime import datetime
from reverence import blue

class Parser(object):
  ''' Class for parsing an EVE cache file into a more convenient format. '''

  def __init__(self, eve_installation_path):
    self._cachemgr = blue.EVE(eve_installation_path).getcachemgr()

  def load_cached_method_call(self, filename):
    ''' Parses the given file using the Reverence cache library. '''

    cache_directory = self._cachemgr.machocachepath
    name = os.path.join(cache_directory, 'CachedMethodCalls', filename)
    return blue.marshal.Load(open(name, 'rb').read())

  def get_cached_method_calls_path(self):
    ''' Returns the directory where all cached method calls are stored. '''
    return os.path.join(self._cachemgr.machocachepath, "CachedMethodCalls")

  def parse(self, filename):
    '''
      Parses the cached EVE method call in the given file returning a higher
      level view of its contents.
      Returns a 2-tuple containing the cache key and the parsed content.
    '''
    try:
      key, data = self.load_cached_method_call(filename)
    except Exception as e:
      raise IOError(e, 'Reverence failed to parse file "{}".'
        .format(filename))

    method_name = key[1]

    if method_name == 'GetOrders':
      region_id, type_id = key[2:]

      rows = ({key: _convert(key, row[key])
        for key in row.__keys__}
        for row in itertools.chain(
          data['lret'][0],
          data['lret'][1],
        )
      )
    elif method_name == 'GetOldPriceHistory':
      region_id, type_id = key[2:]

      # Convert to dicts and manually add typeID and regionID from key
      added_columns = [('typeID', type_id), ('regionID', region_id)]

      rows = (
        dict(added_columns + {key: _convert(key, row[key]) 
        for key in row.__keys__}.items())
        for row in data['lret']
      )
    else:
      # We don't know how to parse this cache file, return an empty result
      rows = (x for x in [])

    return (key, rows)

_UNIT_TIME_OFFSET = 116444736000000000L
def file_system_time_to_unix_time(t):
  ''' 
  Converts the given time in the file system time format,
  a 64-bit value representing the number of 100-nanosecond intervals elapsed
  since 12:00 AM January 1, 1601 UTC to UNIX time.
  '''
  return ((t-_UNIT_TIME_OFFSET) / 10000000L)

def _convert(column, value):
  ''' Applies various conversion to the raw column. '''

  if column in set(['historyDate', 'issueDate']):
    return datetime.fromtimestamp(file_system_time_to_unix_time(value))
  else:
    return value