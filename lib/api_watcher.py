import urllib2
import urllib
import logging
import os
from threading import Thread
from time import sleep
from datetime import datetime, timedelta

from lib.api_parser import Parser
from lib.formating import elapsed_time
from lib.db import insert_rows

class EveApiObserver(object):
  
  def __init__(self, connect, api_key):
    self._api_watcher = EveTransactionWatcher(api_key, self)
    self._connect = connect
    self._conn = None

  def start(self):
    self._api_watcher.start()

  def stop(self):
    self._api_watcher.stop()

  def on_transactions_fetched(self, transactions):
    self._conn = self._conn or self._connect()
    insert_rows(self._conn, 'transactions', transactions)
    logging.info('Inserted %d transactions.', len(transactions))


_API_CACHE_DIR = 'cached_api_calls'
_DATE_FORMAT = '%Y%m%d_%H%M%S'
_TRANSACTIONS_API_URL = 'https://api.eveonline.com/char/WalletTransactions.xml.aspx'

class EveTransactionWatcher(Thread):  
  ''' 
  Queries the EVE api as often as permitted by the
  cached until timestamp for transactions.
  '''

  def __init__(self, api_key, observer):
    Thread.__init__(self)

    assert len(api_key) == 2, 'api_key must be a 2-tuple.'

    self._api_key = api_key
    self._is_interrupted = False
    self._next_update = self._read_cached_until()
    self._observer = observer

  def start(self):
    timeleft = (self._next_update - datetime.utcnow()).total_seconds()
    logging.info('Starting transaction api watcher.')

    if timeleft < 0:
      logging.info('Scheduling query to run immediately.')
    else:
      logging.info('Scheduling query in %s (%s UTC)', elapsed_time(timeleft), self._next_update)
    
    Thread.start(self)

  def run(self):
    while not self._is_interrupted:
      now = datetime.utcnow()

      if now >= self._next_update:
        parameters = {'keyID': self._api_key[0], 'vCode': self._api_key[1]}
        url = 'https://api.eveonline.com/char/WalletTransactions.xml.aspx/?' + urllib.urlencode(parameters)

        logging.info('Querying %s...', url)
        response = urllib2.urlopen(url).read()
        logging.info('Received %d bytes.', len(response))
        xml = unicode(response)

        try:
          parsed = Parser().parse(xml)

          if parsed['error'] is None:
            self._save_response(xml)
            self._observer.on_transactions_fetched(parsed['result']['transactions'])
          else:
            logging.error('Received error "%s" from the eve api.', parsed['error'])
          
          update_delay = parsed['cachedUntil'] - parsed['currentTime']

        except Exception:
          logging.exception('Failed to parse eve api xml.')
          update_delay = timedelta(minutes=10)

        self._next_update = datetime.utcnow() + update_delay
        logging.info('Scheduling new query in %s (%s UTC)', elapsed_time(update_delay.total_seconds()), self._next_update)

    sleep(1.0)

  def stop(self):
    self._is_interrupted = True
    self.join(10000)

  def _save_response(self, data):
    if not os.path.exists(_API_CACHE_DIR):
      os.makedirs(_API_CACHE_DIR)

    filename = 'transactions_{}.xml'.format(datetime.utcnow().strftime(_DATE_FORMAT))
    open(os.path.join(_API_CACHE_DIR, filename), 'w').write(data)

  def _read_cached_until(self):
    get_date = lambda filename: datetime.strptime(filename[filename.index('_') + 1: -4], _DATE_FORMAT)

    try:
      latest_filename = max(os.listdir(_API_CACHE_DIR), key=get_date)
      return Parser().parse(open(os.path.join(_API_CACHE_DIR, latest_filename)).read())['cachedUntil']
    except (ValueError, OSError) as e:
      # No cached files exist
      logging.warn('Could not load cached until date from cached api calls due to. "%s".', e.message)
      return datetime.utcnow()

def main():
  import sqlite3
  import json

  # Configure logging
  logging.basicConfig(
    level=logging.INFO,
    format='[%(levelname)s: %(asctime)s] %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S'
  )

  settings = json.load(open('settings.json'))
  key = (settings['api_key']['id'], settings['api_key']['verification_code'])

  connect = lambda: sqlite3.connect(
    'db.sqlite3',
    detect_types=sqlite3.PARSE_DECLTYPES
  )

  observer = EveApiObserver(connect, key)
  observer.start()

  while True:
    try:
      sleep(0.2)
    except KeyboardInterrupt:
      observer.stop()
      break
  
if __name__ == '__main__':
  main()