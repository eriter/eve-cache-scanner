import sqlite3
from flask import Flask, render_template, Response, json, request, abort, g
from werkzeug.contrib.cache import SimpleCache
from collections import OrderedDict
from datetime import datetime, timedelta

from lib.formating import elapsed_time, format_number, simple_fraction
from lib.tree import generate_tree

class App(Flask):
  def __init__(self, name):
    Flask.__init__(self, name)
    self.market_order_updater = None

app = App(__name__)
cache = SimpleCache()

@app.before_request
def before_request():
  g.db = sqlite3.connect(
    'db.sqlite3',
    detect_types=sqlite3.PARSE_DECLTYPES
  )
  g.db.row_factory = sqlite3.Row

@app.teardown_request
def teardown_request(exception):
  if hasattr(g, 'db'):
    g.db.close()

@app.route('/')
def index():
  return render_template('index.html')

@app.route('/tree', methods=['GET'])
def tree_data():
  response = cache.get('market_tree')

  if response is None:
    market_groups = query('SELECT marketGroupID, parentGroupID, marketGroupName FROM invMarketGroups')
    items = query('SELECT typeID, typeName, marketGroupID FROM invTypes')

    response = jsonify(generate_tree(items, market_groups))
    cache.set('market_tree', response)

  return response

@app.route('/market_updater/<mode>', methods=['PUT'])
def market_updater(mode):
  if mode not in set(['buy', 'sell', 'off']):
    return abort(400)
  elif app.market_order_updater is not None:
    app.market_order_updater.set_market_update_mode(mode)

  return Response(status=200)

# Presets create, delete

@app.route('/presets/create', methods=['POST'])
def create_preset():
  c = g.db.cursor()
  c.execute('INSERT INTO presets (name, selection) VALUES(?, ?)', (request.form['name'], request.form['selection']))
  g.db.commit()
  c.close()

  return jsonify({'id': c.lastrowid})

@app.route('/presets', methods=['GET'])
def get_presets():
  presets = query('SELECT id, name, selection FROM presets')
  return jsonify([{'id': row[0], 'name': row[1], 'selection': json.loads(row[2])} for row in presets])

@app.route('/presets/<int:preset_id>', methods=['DELETE'])
def delete_selection(preset_id):
  query('DELETE FROM presets WHERE id = ?', (preset_id,))
  return Response(status=200)

@app.route('/items', methods=['GET'])
def get_items():
  args = (request.args['name'],) if 'name' in request.args else ()
  where_query = "WHERE typeName like '%?%'" if 'name' in request.args else ''

  return jsonify(query("SELECT typeID AS id, typeName AS name FROM invTypes {} ORDER BY typeName ASC".format(where_query), args))

@app.route('/items/<int:typeID>', methods=['GET'])
def get_item(type_id):
  return jsonify(query("SELECT * FROM invTypes WHERE typeID = ?", (type_id, ), one=True))

@app.route('/regions', methods=['GET'])
def get_regions():
  return jsonify(query("SELECT itemID AS id, itemName AS name FROM mapDenormalize WHERE itemGroup = 'Region' ORDER BY itemName ASC"))

@app.route('/solar_systems', methods=['GET'])
def get_solar_systems():
  solar_systems = query("SELECT itemID, itemName, regionID FROM mapDenormalize WHERE itemGroup = 'Solar System' ORDER BY itemName ASC")

  # Also include the region_id for every solar system
  return jsonify([{'id': {'solar_system_id': row['itemID'], 'region_id': row['regionID']}, 'name': row['itemName']} for row in solar_systems])

# Market history API

@app.route('/history/<int:region_id>/<int:type_id>', methods=['GET'])
def get_history(region_id, type_id):

  if 'sEcho' not in request.args:
    return jsonify(query("SELECT * FROM history WHERE regionID = ? AND typeID = ?", (region_id, type_id)))
  else:
    columns = ['historyDate', 'orders', 'volume', 'lowPrice', 'highPrice', 'avgPrice']
    placeholder_values = [region_id, type_id]
    sql = '''
      SELECT {select} FROM history
      WHERE regionID = ? AND typeID = ?
      {sort}
      {pagination}
    '''

    return handle_datatable_request(
      sql.replace('{select}', ', '.join(columns)),
      columns,
      placeholder_values,
      format_datatable_row,
    )

# Market orders API

@app.route('/orders/<int:region_id>/<int:type_id>', methods=['GET'])
def get_all_orders(region_id, type_id):
  return get_orders_by_type(region_id, type_id, 'all')

@app.route('/orders/<int:region_id>/<int:type_id>/<order_type>', methods=['GET'])
def get_orders_by_type(region_id, type_id, order_type):

  if order_type not in set(['buy', 'sell', 'all']):
    abort(404)

  # Buy, sell or all orders
  bid_query_string = 'AND bid = {}'.format(order_type == 'buy' and 1 or 0) if order_type != 'all' else ''

  if 'sEcho' not in request.args:
    return jsonify(
      query(
        "SELECT * FROM market_orders WHERE market_orders.regionID = ? AND market_orders.typeID = ? {}"
        .format(bid_query_string),
        (region_id, type_id)
      )
    )
  else:
    select_columns = ['volRemaining', 'price', 'mapDenormalize.itemName AS location', 'range', 'minVolume', 'issueDate', 'duration'] if order_type == 'buy' else \
                     ['volRemaining', 'price', 'mapDenormalize.itemName AS location', 'issueDate', 'duration']

    sort_columns   = ['volRemaining', 'price', 'location', 'range', 'minVolume', 'issueDate'] if order_type == 'buy' else \
                     ['volRemaining', 'price', 'location', 'issueDate']

    placeholder_values = [region_id, type_id]
    sql = '''
      SELECT {select}
      FROM market_orders JOIN mapDenormalize on mapDenormalize.itemID = market_orders.stationID
      WHERE market_orders.regionID = ? AND market_orders.typeID = ? {bid}
      {sort}
      {pagination}
    '''

    return handle_datatable_request(
      sql.replace('{select}', ', '.join(select_columns)).replace('{bid}', bid_query_string) ,
      sort_columns,
      placeholder_values,
      format_datatable_row,
    )

# Item breakdown API
@app.route('/item_breakdown/<int:region_id>', methods=['GET'])
def item_breakdown(region_id):

  sql = '''
    SELECT
      sellOrders.typeID,
      typeName,
      MIN(COALESCE(((lowestSellPrice / highestBuyPrice) - 1.0) * avgIskVolume, 0), avgIskVolume) as profit,
      COALESCE(lowestSellPrice / highestBuyPrice, 0) AS margin,
      COALESCE(highestBuyPrice, 0) AS highestBuyPrice,
      COALESCE(lowestSellPrice, 0) AS lowestSellPrice,
      COALESCE(averageSellPrice, 0) AS averageSellPrice,
      COALESCE(averageBuyPrice, 0) AS averageBuyPrice,
      COALESCE(lowestSellPrice / avgHistoryPrice, 0) AS relativeSellPrice,
      COALESCE(highestBuyPrice / avgHistoryPrice, 0) AS relativeBuyPrice,
      COALESCE(supply, 0) AS supply,
      COALESCE(demand, 0) AS demand,
      COALESCE((sellOrders.marketValue + buyOrders.marketValue), 0) AS marketValue,
      COALESCE(avgHistoryPrice, 0) AS avgHistoryPrice,
      COALESCE(avgIskVolume, 0) AS avgIskVolume,
      COALESCE(avgVolume, 0) AS avgVolume,
      COALESCE(CAST(supply AS REAL) / demand, 0) AS marketSupplyDemandRatio,
      COALESCE(avgHistoryPrice / (avgHistoryPriceLow + avgHistoryPriceHigh) + 0.5, 0) AS historySupplyDemandRatio,
      COALESCE((sellOrders.marketValue + buyOrders.marketValue) / avgIskVolume, 0) AS competition
    FROM
      (SELECT
        typeID,
        MIN(price) AS lowestSellPrice,
        SUM(price * volRemaining) / SUM(volRemaining) AS averageSellPrice,
        SUM(volRemaining) AS supply,
        SUM(price * volRemaining) AS marketValue
      FROM market_orders WHERE bid = 0 AND regionID = ? {solar_system} GROUP BY typeID) sellOrders
      LEFT OUTER JOIN
      (SELECT
        typeID,
        MAX(price) AS highestBuyPrice,
        SUM(price * volRemaining) / SUM(volRemaining) AS averageBuyPrice,
        SUM(volRemaining) AS demand,
        SUM(price * volRemaining) AS marketValue
      FROM market_orders WHERE bid = 1 AND regionID = ? {solar_system} GROUP BY typeID) buyOrders
      ON sellOrders.typeID = buyOrders.typeID
      LEFT OUTER JOIN
      (SELECT
        typeID,
        AVG(avgPrice) AS avgHistoryPrice,
        AVG(lowPrice) AS avgHistoryPriceLow,
        AVG(highPrice) AS avgHistoryPriceHigh,
        AVG(avgPrice * history.volume) AS avgIskVolume,
        AVG(history.volume) AS avgVolume
      FROM history WHERE regionID = ? {history_filter} GROUP by typeID) history
      ON sellOrders.typeID = history.typeID
      LEFT OUTER JOIN invTypes ON sellOrders.typeID = invTypes.typeID
    {filter}
    {sort}
    {pagination}
  '''

  # Filter on a specific solar system or not
  solar_system_query_string = 'AND market_orders.solarSystemID = ?' if 'solar_system_id' in request.args else ''

  if 'solar_system_id' in request.args:
    placeholder_values = [region_id, request.args['solar_system_id'], region_id, request.args['solar_system_id'], region_id]
  else:
    placeholder_values = [region_id, region_id, region_id]

  # Cut of history at a specific date or not
  history_query_string = ''
  if 'history_after' in request.args:
    placeholder_values.append(request.args['history_after'])
    history_query_string = 'AND historyDate >= datetime(?)'

  if 'sEcho' not in request.args:
    sql = sql.format(
      solar_system=solar_system_query_string,
      sort='',
      filter='',
      history_filter=history_query_string,
      pagination=''
    )

    return jsonify(query(sql, tuple(placeholder_values)))

  else:
    # Get total records
    count_query = "SELECT COUNT(*) FROM (SELECT 1 FROM market_orders WHERE regionID = ? {solar_system} AND bid = 0 GROUP BY typeID)"
    count_query_args = [region_id]

    if 'solar_system_id' in request.args:
      count_query = count_query.format(solar_system='AND solarSystemID = ?')
      count_query_args.append(request.args.get('solar_system_id', type=int))

    total_records = query(count_query, tuple(count_query_args), one=True)[0]

    # Filters on the end result
    filter_conjunctions = []

    filters = {
      'margin': 'margin',
      'isk_volume': 'avgIskVolume',
      'buy_price' : 'highestBuyPrice',
      'sell_price': 'lowestSellPrice',
      'volume': 'avgVolume',
    }

    for k, column in filters.iteritems():
      min_key = k + '_min'
      max_key = k + '_max'

      if min_key in request.args:
        filter_conjunctions.append('{} >= ?'.format(column))
        placeholder_values.append(request.args.get(min_key, type=float))

      if max_key in request.args:
        filter_conjunctions.append('{} <= ?'.format(column))
        placeholder_values.append(request.args.get(max_key, type=float))

    filter_query_string = 'WHERE {}'.format(' AND '.join(filter_conjunctions)) if len(filter_conjunctions) > 0 else ''

    return handle_datatable_request(
      sql.replace('{filter}', filter_query_string).replace('{solar_system}', solar_system_query_string).replace('{history_filter}', history_query_string),
      ['invTypes.typeName', 'profit', 'margin', 'avgIskVolume',  'avgVolume', \
       'lowestSellPrice', 'highestBuyPrice', 'avgHistoryPrice', 'relativeSellPrice', \
       'relativeBuyPrice', 'marketSupplyDemandRatio', 'historySupplyDemandRatio', 'competition'],
      placeholder_values,
      get_item_breakdown_datatable_row,
      total_records,
    )

def get_item_breakdown_datatable_row(row):
  return format_datatable_row([
    ('typeName', row['typeName']),
    ('profit', row['profit']),
    ('margin', '{:+.1%}'.format(row['margin'] - 1.0) if row['margin'] != 0 else '-'),
    ('iskVolume,', row['avgIskVolume']),
    ('volume', row['avgVolume']),
    ('sellPrice', row['lowestSellPrice']),
    ('buyPrice', row['highestBuyPrice']),
    ('avgPrice', row['avgHistoryPrice']),
    ('relativeSellPrice', '{:+.1%}'.format(row['relativeSellPrice'] - 1.0) if row['relativeSellPrice'] != 0 else '-'),
    ('relativeBuyPrice', '{:+.1%}'.format(row['relativeBuyPrice'] -1.0) if row['relativeBuyPrice'] != 0 else '-'),
    ('marketSupplyDemand', simple_fraction(row['marketSupplyDemandRatio'])),
    ('historySupplyDemand', simple_fraction(row['historySupplyDemandRatio'])),
    ('competition', row['competition'])
  ])

@app.route('/transactions/profit_by_item', methods=['GET'])
def profit_by_item():
  sql = '''
    SELECT
      buy.typeName AS typeName,
      avgBuyPrice,
      avgSellPrice,
      bought,
      sold,
      MAX(0, bought - sold) AS unsold,
      COALESCE(avgSellPrice / avgBuyPrice, 0) AS margin,
      (received - spent) + (bought - sold) * avgSellPrice AS profit
    FROM
      (SELECT
        typeName,
        AVG(price) AS avgBuyPrice,
        SUM(quantity) AS bought,
        SUM(price * quantity) AS spent
      FROM transactions WHERE {transactionDate} transactionType = 'buy' GROUP BY typeName) buy
      JOIN
      (SELECT
        typeName,
        AVG(price) AS avgSellPrice,
        SUM(quantity) AS sold,
        SUM(price * quantity) AS received
      FROM transactions WHERE {transactionDate} transactionType = 'sell' GROUP BY typeName) sell
      ON buy.typeName = sell.typeName
    {sort}
    {pagination}
  '''
  placeholder_values = []
  placeholder_values_date = []

  # Transaction constraints
  transaction_date_constraints = []

  if 'transactions_start' in request.args:
    placeholder_values.append(request.args['transactions_start'])
    placeholder_values.append(request.args['transactions_start'])
    placeholder_values_date.append(request.args['transactions_start'])
    transaction_date_constraints.append('transactionDateTime >= datetime(?)')

  if 'transactions_end' in request.args:
    placeholder_values.append(request.args['transactions_end'])
    placeholder_values.append(request.args['transactions_end'])
    placeholder_values_date.append(request.args['transactions_end'])
    transaction_date_constraints.append('transactionDateTime <= datetime(?)')

  transaction_date_query = 'AND '.join(transaction_date_constraints) + ' AND ' if len(transaction_date_constraints) > 0 else ''

  if 'sEcho' not in request.args:
    return jsonify(query(sql.format(sort='', pagination='', transactionDate=transaction_date_query), tuple(placeholder_values)))
  else:
    total_records = query("SELECT COUNT(*) FROM (SELECT 1 FROM transactions WHERE transactionType = 'buy' GROUP BY typeName)", one=True)[0]

    # Get earliest and latest transaction date
    dates = query('''
      SELECT
        datetime(MIN(transactionDateTime)) AS earliestTransactionDate,
        datetime(MAX(transactionDateTime)) AS latestTransactionDate
        FROM transactions {}
      '''.format(('WHERE ' + 'AND '.join(transaction_date_constraints)) if len(transaction_date_constraints) > 0 else ''),
      tuple(placeholder_values_date),
      one=True
    )

    result = get_datatable_result(
      sql.replace('{transactionDate}', transaction_date_query),
      ['typeName', 'profit', 'margin', 'avgSellPrice',  'avgBuyPrice', 'sold', 'bought', 'unsold'],
      placeholder_values,
    )

    # Summarize total profit
    result = [row for row in result]
    total_profit = sum(row['profit'] for row in result)
    profit_per_month = 0

    earliest_date = dates['earliestTransactionDate']
    latest_date = dates['latestTransactionDate']

    if earliest_date is not None and latest_date is not None:
      # Keep it simple, 1 month = 30 days, we're only interested in estimates anyway
      months = (datetime.strptime(latest_date, '%Y-%m-%d %H:%M:%S') - datetime.strptime(earliest_date, '%Y-%m-%d %H:%M:%S')).total_seconds() / (30.0 * 24.0 * 3600.0)
      profit_per_month = total_profit / months if months != 0 else 0

    result = [get_profit_breakdown_datatable_row(row) for row in result]

    return jsonify({
      'iTotalRecords': total_records,
      'iTotalDisplayRecords': total_records,
      'aaData': result,
      'earliest_transaction': dates['earliestTransactionDate'],
      'latest_transaction': dates['latestTransactionDate'],
      'total_profit': format_number(total_profit),
      'profit_per_month': format_number(profit_per_month),
      'sEcho': request.args.get('sEcho', type=int)
    })

def get_profit_breakdown_datatable_row(row):
  return format_datatable_row([
    ('typeName', row['typeName']),
    ('profit', row['profit']),
    ('margin', '{:+.1%}'.format(row['margin'] - 1.0) if row['margin'] != 0 else '-'),
    ('sellPrice', row['avgSellPrice']),
    ('buyPrice', row['avgBuyPrice']),
    ('sold', row['sold']),
    ('bought', row['bought']),
    ('unsold', row['unsold']),
  ])

def format_datatable_row(row):
  # Convert to dict as sqlite3.Row's are not editable
  row = OrderedDict(row)

  for key, value in row.iteritems():
    # Based on column name
    if key == 'issueDate':
      # Convert to 'Expires in' string using the duration
      row[key] = elapsed_time(int(((value + timedelta(days=row['duration'])) - datetime.utcnow()).total_seconds()))

    elif key == 'range':
      # Convert range to string
      ranges = {-1: 'Station', 0: 'Solar System', 32767: 'Region'}
      row[key] = ranges[value] if value in ranges else '{:d} jumps'.format(value)

    # Generic conversions
    elif isinstance(value, (int, long, float)):
      row[key] = format_number(value)

    elif isinstance(value, datetime):
      row[key] = value.strftime('%Y-%m-%d %H:%M:%S')

  return row.values()

def get_datatable_result(sql, columns, placeholder_values):
  sort_col    = request.args.get('iSortCol_0', type=int)
  sort_dir    = request.args['sSortDir_0']
  limit       = request.args.get('iDisplayLength', type=int)
  offset      = request.args.get('iDisplayStart', type=int)

  sort_query = 'ORDER BY {column} {direction}'.format(column=columns[sort_col], direction=(sort_dir == 'asc' and 'ASC' or 'DESC'))

  # Pagination
  pagination_query = 'LIMIT ? OFFSET ?'
  placeholder_values.append(limit)
  placeholder_values.append(offset)

  return query(sql.format(sort=sort_query, pagination=pagination_query), tuple(placeholder_values))

def handle_datatable_request(sql, columns, placeholder_values, row_processor, total_records=None):
  result = [row_processor(row) for row in get_datatable_result(sql, columns, placeholder_values)]
  length = len(result) if total_records is None else total_records

  return jsonify({
    'iTotalRecords': length,
    'iTotalDisplayRecords': length,
    'aaData': result,
    'sEcho': request.args.get('sEcho', type=int)
  })

def query(sql, args=(), one=False):
  c = g.db.cursor()
  c.execute(sql, args)

  try:
    return c.fetchone() if one else c.fetchall()
  finally:
    c.close()
    g.db.commit()

# Custom jsonify function that supports arrays as root and can serialize datetimes and sqlite3.Row's
def jsonify(data):
  '''
  Custom jsonify function that supports arrays as root and can serialize
  datetimes and sqlite3.Row's.
  '''
  def json_encode(obj):
    if isinstance(obj, datetime):
      return obj.strftime('%Y-%m-%d %H:%M:%S')
    elif isinstance(obj, sqlite3.Row):
      return {key: obj[key] for key in obj.keys()}
    else:
      return None

  return Response(json.dumps(data, default=json_encode), status=200, mimetype='application/json')

if __name__ == '__main__':
  app.run(host='0.0.0.0', debug=True, threaded=True)
